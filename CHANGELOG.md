# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/ucore-project/Core/compare/v1.0.0...v1.0.1) (2020-08-21)


### Bug Fixes

* **Model:** fix find() & first() return type ([796ed83](https://gitlab.com/ucore-project/Core/commit/796ed830779fb18871058c314eadd18f79001792))

## 1.0.0 (2020-08-21)


### Features

* add uCore\Core ([3560b40](https://gitlab.com/ucore-project/Core/commit/3560b40fe63b48d59cf30ef474fb98a7249f7afe))
