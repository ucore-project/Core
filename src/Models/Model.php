<?php

namespace uCore\Core\Models;

use uCore\Core\Entities\Entity;

class Model extends \CodeIgniter\Model
{
    protected $returnType = 'uCore\Core\Entities\Entity';

    public function __construct()
    {
        parent::__construct();

        helper('array');

        $this->createdField  = 'createdAt';
        $this->updatedField  = 'updatedAt';
        $this->deletedField  = 'deletedAt';

        $this->afterFind = ['resultFormatter'];
    }

    /**
     * Get model's table name
     *
     * @return string|null
     */
    public function getTable(): ?string
    {
        return $this->table;
    }

    /**
     * Get model's allowed fields
     *
     * @return array|null
     */
    public function getAllowedFields(): ?array
    {
        return $this->allowedFields;
    }

    /**
     * Validate class cast fix
     *
     * @param mixed $data
     * @return boolean
     */
    public function validate($data): bool
    {
        if (is_object($data) && !$data instanceof \stdClass) {
            $data = static::classToArray($data, $this->primaryKey, $this->dateFormat, false);
        }

        if (array_key_exists($this->primaryKey, $data) && empty($data[$this->primaryKey])) {
            unset($data[$this->primaryKey]);
        }

        return parent::validate($data);
    }

    public function chunk(int $size, \Closure $userFunc)
    {
        $total = $this->builder()
            ->countAllResults(false);

        $offset = 0;
        $builder = clone ($this->builder());

        while ($offset <= $total) {
            $rows = $builder->get($size, $offset, FALSE);

            if ($rows === false) {
                throw \CodeIgniter\Database\Exceptions\DataException::forEmptyDataset('chunk');
            }

            $rows = $rows->getResult($this->tempReturnType);

            $offset += $size;

            if (empty($rows)) {
                continue;
            }

            foreach ($rows as $row) {
                $eventData = $this->trigger('afterFind', ['data' => $row]);

                if ($userFunc($eventData['data']) === FALSE) {
                    return;
                }
            }
        }
    }

    protected function resultFormatter($arg): array
    {
        $this->builder()->from($this->table, TRUE);

        if (is_array($arg['data'])) {
            $arg['data'] = array_map(
                function (Entity $row) {
                    return $this->rowFormatter($row);
                },
                $arg['data']
            );
        } else if (is_object($arg['data'])) {
            $arg['data'] = $this->rowFormatter($arg['data']);
        }

        return $arg;
    }

    protected function rowFormatter(Entity $row): Entity
    {
        return $row;
    }

    public function find($id = null): ?Entity
    {
        return parent::find($id);
    }

    public function first(): ?Entity
    {
        return parent::first();
    }

    /**
     * Convert domain array to query builder
     *
     * @param array|null $domain
     * @param string $case
     * @return Model
     */
    public function domain(?array $domain, string $case = "&"): Model
    {
        $builder = $this->builder();

        foreach ($domain as $filter) {
            if (is_array($filter)) {
                if (is_string($filter[0])) {
                    if (in_array($filter[0], array("&", "|"))) {
                        if (is_array($filter[1])) {
                            if ($filter[0] == "&") {
                                $builder->groupStart();
                            } else {
                                $builder->orGroupStart();
                            }

                            $this->domain(array($filter[1]), $filter[0]);
                            $builder->groupEnd();

                            continue;
                        }

                        $this->_parseDomain($filter[0], $filter[1], $filter[2], $filter[3], (count($filter) == 5 ? $filter[4] : NULL));
                    } else {
                        $this->_parseDomain($case, $filter[0], $filter[1], $filter[2], (count($filter) == 4 ? $filter[3] : NULL));
                    }
                } else {
                    if ($case == "&") {
                        $builder->groupStart();
                    } else {
                        $builder->orGroupStart();
                    }

                    $this->domain($filter, $case);
                    $builder->groupEnd();
                }
            }
        }

        return $this;
    }

    /**
     * Translates domain operator to CI query builder
     *
     * @param string $case
     * @param string $field
     * @param string $operator
     * @param mixed $value
     * @param bool $escape
     * @return void
     */
    private function _parseDomain($case, $field, $operator, $value, $escape = NULL): Model
    {
        $builder = $this->builder();
        $operator = strtolower($operator);

        switch ($operator) {
            case "=":
            case "!=":
            case "<>":
            case ">":
            case ">=":
            case "<":
            case "<=":
                if ($case == "&")
                    $builder->where("{$field} {$operator}", $value, $escape);
                elseif ($case == "|")
                    $builder->orWhere("{$field} {$operator}", $value, $escape);
                break;

            case "in":
                if ($case == "&")
                    $builder->whereIn("{$field}", $value, $escape);
                elseif ($case == "|")
                    $builder->orWhereIn("{$field}", $value, $escape);
                break;

            case "not_in":
                if ($case == "&")
                    $builder->whereNotIn("{$field}", $value, $escape);
                elseif ($case == "|")
                    $builder->orWhereNotIn("{$field}", $value, $escape);
                break;

            case "like":
                if ($case == "&")
                    $builder->like("{$field}", $value, "both", $escape);
                elseif ($case == "|")
                    $builder->orLike("{$field}", $value, "both", $escape);
                break;

            case "start_like":
                if ($case == "&")
                    $builder->like("{$field}", $value, "after", $escape);
                elseif ($case == "|")
                    $builder->orLike("{$field}", $value, "after", $escape);
                break;

            case "end_like":
                if ($case == "&")
                    $builder->like("{$field}", $value, "before", $escape);
                elseif ($case == "|")
                    $builder->orLike("{$field}", $value, "before", $escape);
                break;
        }

        return $this;
    }

    /**
     * Converts select array to CI's query builder
     *
     * @param array|string $selects
     * @param null|string $alias
     * @return Model
     */
    public function select($selects, ?string $alias = NULL): Model
    {
        $builder = $this->builder();

        if (is_string($selects)) {
            $alias = $alias ? "AS {$alias}" : NULL;

            $builder->select("{$selects} {$alias}");
        } else if (is_array($selects)) {
            foreach ($selects as $select_field => $select_alias) {
                if (is_integer($select_field)) {
                    $this->select($select_alias);
                } else {
                    $this->select($select_field, $select_alias);
                }
            }
        }

        return $this;
    }

    /**
     * Converts join array to CI's query builder
     *
     * @param array $joins
     * @return Model
     */
    public function join(array $joins): Model
    {
        $builder = $this->builder();

        foreach ($joins as $_join) {
            $joinTable = isset($_join[0]) ? $_join[0] : NULL;
            $joinOn = isset($_join[1]) ? $_join[1] : NULL;
            $joinDir = isset($_join[2]) ? $_join[2] : "";

            if ($joinTable != NULL && $joinOn != NULL) {
                $builder->join($joinTable, $joinOn, $joinDir);
            }
        }

        return $this;
    }

    /**
     * Converts limit array to CI's query builder
     *
     * @param array|string|int $limit
     * @param integer $offset
     * @return Model
     */
    public function limit($limit, int $offset = 0): Model
    {
        $builder = $this->builder();

        if (is_integer($limit)) {
            $builder->limit($limit, $offset);
        } else if (is_array($limit)) {
            $this->limit($limit[0], $limit[1]);
        }

        return $this;
    }

    /**
     * Converts order array to CI's query builder
     *
     * @param string|array $orders
     * @param string $direction
     * @param bool $escape
     * @return Model
     */
    public function orderBy($orders, string $direction = "ASC", bool $escape = TRUE): Model
    {
        $builder = $this->builder();

        if ($orders == "random") {
            $builder->orderBy("RAND()", "", FALSE);
            return $this;
        }

        if (is_string($orders)) {
            $builder->orderBy($orders, $direction, $escape);
        } else if (is_array($orders)) {
            foreach ($orders as $order_field => $order_direction) {
                $this->orderBy($order_field, $order_direction, $escape);
            }
        }

        return $this;
    }

    /**
     * Converts group array to CI's query builder
     *
     * @param string|array $group_by
     * @return Model
     */
    public function groupBy($group_by): Model
    {
        $builder = $this->builder();

        if (is_string($group_by)) {
            $builder->groupBy($group_by);
        } else if (is_array($group_by)) {
            foreach ($group_by as $group_field) {
                $this->groupBy($group_field);
            }
        }

        return $this;
    }

    /**
     * Converts having array to CI's query builder
     *
     * @param string|array $having
     * @return Model
     */
    public function having($having): Model
    {
        $builder = $this->builder();

        if (is_string($having)) {
            $builder->having($having);
        } else if (is_array($having)) {
            foreach ($having as $hv) {
                $this->having($hv);
            }
        }

        return $this;
    }
}
