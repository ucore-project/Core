<?php
function dot_object_search(string $index, object $object)
{
    $segments = explode('.', rtrim(rtrim($index, '* '), '.'));

    return _object_search_dot($segments, $object);
}

function _object_search_dot(array $indexes, object $object)
{
    $currentIndex = $indexes
        ? array_shift($indexes)
        : null;

    if ((empty($currentIndex)  && intval($currentIndex) !== 0) || (!isset($object->{$currentIndex}) && $currentIndex !== '*')) {
        return null;
    }

    if (empty($indexes)) {
        return $object->{$currentIndex};
    }

    if (is_object($object->{$currentIndex}) && $object->{$currentIndex}) {
        return _object_search_dot($indexes, $object->{$currentIndex});
    }

    return $object->{$currentIndex};
}

function dot_search(string $key, $source, $default = NULL)
{
    if (is_array($source)) {
        return dot_array_search($key, $source) ?? $default;
    } else if (is_object($source)) {
        return dot_object_search($key, $source) ?? $default;
    } else {
        return $default;
    }
}
